const skills = {
    en: [
        {title: "React", image_url: "https://i.imgur.com/SvI3qR5.png", job_experience:"1 year of job experience", extra: "Extra experience with hobby control panel project frontend"},
        {title: "React-Native", image_url: "https://i.imgur.com/EqkHVpW.png",  job_experience:"1 year of job experience. Tablet app for IoT project and conversion to Huawei", extra: "Extra experience from Vegaaniskanneri hobby project"},
        {title: "Java", image_url: "https://i.imgur.com/baUR3Z1.png", job_experience:"1,5 year of job experience. Mainly using spring framework in an IoT project", extra: "Small experience on home projects using spring framework"},
        {title: "Python", image_url: "https://i.imgur.com/oFfJzXL.png", job_experience:"1 year of job experience. Part of crafting a solution for driving LED-wall, sound and video in a controlled environment", extra: "Some experience with hobby light control panel project backend"},
        {title: "Dart", image_url: "https://i.imgur.com/WaT3Z7b.png", job_experience:"No formal experience", extra: "Some experience with photo challenge hobby project"},
        {title: "Android", image_url: "https://i.imgur.com/j4iGx33.png", job_experience: "Some native android code for a React native project", extra: "Familiar with the android release process from Vegaaniskanneri project"},
        {title: "iOS", image_url: "https://i.imgur.com/GNRC5U1.png", job_experience: "No native iOS job experience", extra: "Familiar with Xcode and apple store release process from Vegaaniskanneri project"},
        {title: "More...", image_url: "https://i.imgur.com/PZGploc.png", job_experience: "Experience with various other tools; Docker, Express, Django, Windows, Linux, macOS, Git, GitLab CI, Jira, MQTT, Mosquitto, InfluxDB, PostgreSQL, MongoDB, etc..."}
    ],
    fi: [
        {title: "React", image_url: "https://i.imgur.com/SvI3qR5.png", job_experience:"1 vuoden työkokemus", extra: "Lisäkokemusta hallintapaneeli-harrasteprojektin frontendin kautta"},
        {title: "React-Native", image_url: "https://i.imgur.com/EqkHVpW.png",  job_experience:"1 vuoden työkokemus. Tablet-sovellus IoT projektille ja Huawei media services käännös", extra: "Lisäkokemusta Vegaaniskanneri-harrasteprojektista"},
        {title: "Java", image_url: "https://i.imgur.com/baUR3Z1.png", job_experience:"1,5 vuoden työkokemus. Pääasiassa Spring Frameworkillä IoT projektissa.", extra: "Vähäinen liäkokemus kesken jääneen harrasteprojektin spring framework backendistä"},
        {title: "Python", image_url: "https://i.imgur.com/oFfJzXL.png", job_experience:"1 vuoden työkokemus. Mukana tekemässä sovelluskokonaisuutta jolla hallitaan tilan LED-seinää, audiota ja videota.", extra: "Lisäkokemusta ohjauspaneeli-harrasteprojektin backend"},
        {title: "Dart", image_url: "https://i.imgur.com/WaT3Z7b.png", job_experience:"Ei virallista työkokemusta", extra: "Kokemusta kuvahaaste-harrasteprojektin kautta"},
        {title: "Android", image_url: "https://i.imgur.com/j4iGx33.png", job_experience: "Parin Natiivi android moduulin teko react-nativeen.", extra: "Android julkaisuprosessi ja Play Console työkalut tuttuja Vegaaniskanneri-projektista"},
        {title: "iOS", image_url: "https://i.imgur.com/GNRC5U1.png", job_experience: "Ei natiivi iOS/Swift", extra: "Xcode, App Store Connect ja Testflight tuttuja Vegaaniskanneri-projektista"},
        {title: "Lisää...", image_url: "https://i.imgur.com/PZGploc.png", job_experience: "Kokemusta monista muista työkaluista ja alustoista; Docker, Express, Django, Windows, Linux, macOS, Git, GitLab CI, Jira, MQTT, Mosquitto, InfluxDB, PostgreSQL, MongoDB, yms..."},
    ]
}

module.exports = {
    skills: skills
};