const express = require('express');
const app = express();
const port =  process.env.PORT || 3000;
const skills = require('./content/skills').skills;

app.get('/skills/:lang', (req, res) => {
  let lang = req.params.lang;
  res.json(skills[lang])
})

app.listen(port, () => {
  console.log(`React-native-showcase-api app listening at http://localhost:${port}`)
})